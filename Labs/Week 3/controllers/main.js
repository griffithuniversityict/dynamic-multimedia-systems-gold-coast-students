/********************************************************
 *                      Main.js
 * This file sets the initial state for the web app
 * The controller files are responsible for the interactions
 * between the model and the view layers. Ideally, you will 
 * have a new controller for each HTML page.
 * 
 *******************************************************/

// Do stuff when the window loads
window.onload = function() {
    // Get all the images and call display function
    //... code goes here

    // Listen for the search button event and call search function
    //... code goes here
};

// Search Images
function searchImages() {
    // Get the keyword from the form
    // Call the repeat function and display results
    //... code goes here
}

// Choose what images to display
function displayImages(images, search = false) {
    
    /**************
     * This function is the trickiest as it's about the DOM
     * You need to be able to create rows, image containers, images and captions. 
     * If you are using Bootstrap, you will probably have to consider a counter
     * ask your tutor for ideas if you get stuck
     * 
     * 
     * Notes: the search parameter is defaulted to "false", remember to set to 
     * TRUE when you are calling this function through the search function so
     * that you can set the CSS background correctly
     * 
     * Remember to use the following:
     *      var row = document.createElement("div")
     *      row.setAttribute("class", "row")
     * 
     *      var item = document.createElement("div");
     *      row.appendChild(item);
     ***********/
     
     
    //... code goes here
}

// This function clears the body section
function clearGallery() {
    //... code goes here
}

// This function presents the first image in the array for N times
function repeat(n, images) {
    //... code goes here
}

// Maintains an array of all the images stored for the lab
function getImages() {
    var images = [{
        fname: 'DSC01049.jpg',
        title: 'City View'
    }, {
        fname: 'DSC01066.jpg',
        title: 'Ferris Wheel'
    }, {
        fname: 'DSC02511.jpg',
        title: 'A building in the forbidden city with extra long text'
    }, {
        fname: 'DSC03810.jpg',
        title: 'City from Mt Gravatt'

    }, {
        fname: 'DSC05750.jpg',
        title: 'Sunrise?'
    }];

    var imgArr = [];
    for (var i = 0; i < images.length; i++) {
        var item = images[i];
        var img = new Image(i + 1, item.title, item.title, item.fname);
        imgArr.push(img);
    }

    return imgArr;
}